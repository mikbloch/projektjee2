package com.enterprise.projekt2.controller;

import com.enterprise.projekt2.domain.Message;
import com.enterprise.projekt2.domain.Person;
import com.enterprise.projekt2.service.MessageManager;
import com.enterprise.projekt2.service.PersonManager;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class PersonController {
    @Autowired
    PersonManager pm;
    @Autowired
    MessageManager mm;

    @GetMapping("/signup")
    public String signup(final Model model) {
        model.addAttribute("Person", new Person());
        model.addAttribute("error", false);
        return "signup";
    }

    @PostMapping("/signup")
    public String create(@Valid Person person, Errors errors, Model model){
        if(errors.hasErrors()){
            model.addAttribute("error", true);
            model.addAttribute("Person", new Person());
            return "signup";
        }
        pm.save(person);
        return "redirect:/person/" + person.getId();
    }


    @GetMapping("/person/{id}/edit")
    public String edit(@PathVariable int id, final Model model){
        model.addAttribute("person", pm.getOne(id));
        return "editperson";
    }

    @PostMapping("/edit-person/{id}")
    public String update(Person person, @PathVariable int id) {
        Person persontoupdate = pm.getOne(id);
        persontoupdate.setNick(person.getNick());
        persontoupdate.setEmail(person.getEmail());
        persontoupdate.setPassword(person.getPassword());
        pm.save(persontoupdate);
        return "redirect:/person/" + person.getId();
    }

    @GetMapping("/delete-person/{id}")
    public String delete(@PathVariable int id) {
        pm.deleteById(id);
        return "redirect:/";
    }
}
