package com.enterprise.projekt2.controller;

import com.enterprise.projekt2.Saveing;
import com.enterprise.projekt2.domain.Person;
import com.enterprise.projekt2.service.MessageManager;
import com.enterprise.projekt2.service.PersonManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class AdminController {
    @Autowired
    PersonManager pm;
    @Autowired
    MessageManager mm;
    @Autowired
    Saveing sv;

    @GetMapping("/admin")
    public String home(@RequestParam(required = false) String query, final Model model){
        List<Person> persons =
                query==null ? pm.findAll() : pm.findByNickContains(query);
        model.addAttribute("persons", persons);
        model.addAttribute("query", query);
        return "admin";
    }

    @GetMapping("/savejsonpeople")
    public ResponseEntity<Resource> savejsonpeople() {
        Resource file = sv.downloadJson("people");
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"Persons.json\"")
                .body(file);
    }

    @GetMapping("/savejsonmessages")
    public ResponseEntity<Resource> savejsonmessages() {
        Resource file = sv.downloadJson("messages");
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"Messeges.json\"")
                .body(file);
    }

}
