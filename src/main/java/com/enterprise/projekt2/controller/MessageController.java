package com.enterprise.projekt2.controller;

import com.enterprise.projekt2.domain.Message;
import com.enterprise.projekt2.domain.Person;
import com.enterprise.projekt2.service.MessageManager;
import com.enterprise.projekt2.service.PersonManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.sql.Date;
import java.util.Calendar;

@Controller
public class MessageController {
    @Autowired
    PersonManager pm;
    @Autowired
    MessageManager mm;

    @GetMapping("/person/{id}")
    public String read(@PathVariable int id, Model model, @RequestParam(required = false) String admin) {
        model.addAttribute("error", false);
        model.addAttribute("post", new Message());
        model.addAttribute("id", id);
        model.addAttribute("messages", mm.findAllByPerson_Id(id));
        model.addAttribute("admin", admin!=null);
        return "person";
    }

    @PostMapping("/person/{id}")
    public String create(@Valid Message message, Errors errors, @PathVariable int id, Model model) {
            if(errors.hasErrors()){
            model.addAttribute("error", true);
            model.addAttribute("post", new Message());
            return "/person/" + id;
        }
        message.setPerson(pm.getById(id));
        message.setCreate_date(new Date(Calendar.getInstance().getTimeInMillis()));
        mm.save(message);
        model.addAttribute("error", false);
        return "redirect:/person/" + id;
    }

    @GetMapping("delete-post/{id}")
    public String delete(@PathVariable int id) {
        int personid = mm.getOne(id).getPerson().getId();
        mm.deleteById(id);
        return "redirect:/person/" + personid;
    }

    @GetMapping("/person/{pid}/edit/{mid}")
    public String editpost(@PathVariable int pid, @PathVariable int mid, Model model) {
        model.addAttribute("message", mm.getOne(mid));
        return "editpost";
    }

    @GetMapping("/person/{pid}/edit/{mid}/save")
    public ResponseEntity<Resource> savepost(@PathVariable int pid, @PathVariable int mid) {
        String data = mm.getOne(mid).getContent();
        Resource file = new ByteArrayResource(data.getBytes());
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"Messeges.txt   \"")
                .body(file);

    }

    @PostMapping("/edit-post/{id}")
    public String update(Message message, @PathVariable int id) {
        Message oldmessage = mm.getOne(id);
        oldmessage.setContent(message.getContent());
        mm.save(oldmessage);
        return "redirect:/person/" + oldmessage.getPerson().getId();
    }
}
