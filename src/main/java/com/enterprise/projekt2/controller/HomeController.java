package com.enterprise.projekt2.controller;

import com.enterprise.projekt2.domain.Message;
import com.enterprise.projekt2.domain.Person;
import com.enterprise.projekt2.service.MessageManager;
import com.enterprise.projekt2.service.PersonManager;
import com.sun.corba.se.pept.protocol.MessageMediator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

@Controller
public class HomeController {

    @Autowired
    PersonManager pm;
    @Autowired
    MessageManager mm;

    @GetMapping("/")
    public String home(final Model model, @RequestParam(required = false) String qp, @RequestParam(required = false) String qm, @RequestParam(required = false) Integer user) {
        List<Message> messages = null;
        if (qp!=null||qm!=null) {
            if (qp == null)
                qp = "";
            if (qm == null)
                qm = "";
            messages = mm.findAllByContentContainsAndPerson_NickContains(qm, qp);
        }
        else
            messages = mm.findAll();
        model.addAttribute("messages", messages);
        model.addAttribute("user", user);
        model.addAttribute("qp", qp);
        model.addAttribute("qm", qm);
        return "home";
    }

    @GetMapping("/login")
    public String login(final Model model) {
        model.addAttribute("nick", new String());
        model.addAttribute("password", new String());
        return "login";
    }

    @PostMapping("/login-check")
    public String logincheck(String nick, String password){
        Person person = pm.getByNick(nick);
        if (nick.equals("admin") && password.equals("admin")) {
            return "redirect:/admin";
        }
        if (person != null) {
            if (person.getPassword().equals(password))
                return "redirect:/person/" + person.getId();
            return "login";
        }
        return "login";
    }
}
