package com.enterprise.projekt2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;

@Data
@Entity
public class Person {
    @Id
    @GeneratedValue
    private int id;
    @NotBlank(message = "nie może być puste")
    private String nick;
    @NotBlank(message = "nie może być puste")
    @Pattern(regexp = "[A-Za-z0-9]*", message = "hasło musi składać sie tylko z liter/liczb")
    @Pattern(regexp = ".{6,}", message = "hasło musi składać sie z przynajmniej 6 znaków")
    private String password;
    @NotBlank(message = "nie może być puste")
    @Pattern(regexp = "[a-zA-Z][a-zA-Z0-9]*@[a-zA-Z][a-zA-Z0-9]*[.][a-zA-Z]+", message = "podaj poprawny email")
    private String email;
    @JsonIgnore
    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL, orphanRemoval = true)
    List<Message> messages;
}
