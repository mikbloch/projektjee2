package com.enterprise.projekt2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.sql.Date;

@Data
@Entity
public class Message {
    @Id
    @GeneratedValue
    private int id;
    @NotBlank(message = "wiadomość nie pusta")
    @Size(max = 144, message = "maksymalna wielkość wiadomości to 144")
    private String content;
    private Date create_date;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    Person person;
}