package com.enterprise.projekt2;

import com.enterprise.projekt2.domain.Message;
import com.enterprise.projekt2.domain.Person;
import com.enterprise.projekt2.service.MessageManager;
import com.enterprise.projekt2.service.PersonManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;


import java.io.File;
import java.io.IOException;
import java.util.List;

@Component
public class Saveing {
    @Autowired
    MessageManager mm;
    @Autowired
    PersonManager pm;

    ObjectMapper om = new ObjectMapper();

    private String getDataPeople() {
        List<Person> people = pm.findAll();

        String s = "{\"people\":";

        for (Person person : people
        ) {
            try {
                s += om.writeValueAsString(person);
            }
            catch (IOException e){
                e.printStackTrace();
            }
            if (people.get(people.size()-1) != person)
                s+=",";
        }
        s+="}";
        return s;
    }

    private String getDataMesseges() {
        List<Message> messages = mm.findAll();

        String s = "{\"messages\":";

        for (Message message : messages
        ) {
            try {
                s += om.writeValueAsString(message);
            }
            catch (IOException e){
                e.printStackTrace();
            }
            if (messages.get(messages.size()-1) != message)
                s+=",";
        }
        s+="}";
        return s;
    }


    public Resource downloadJson(String name){
        String data = null;
        if (name.equals("people"))
            data = getDataPeople();
        else if (name.equals("messages"))
            data = getDataMesseges();

        return new ByteArrayResource(data.getBytes());
    }
}
