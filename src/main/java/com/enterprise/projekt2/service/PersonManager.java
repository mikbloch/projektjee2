package com.enterprise.projekt2.service;

import com.enterprise.projekt2.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonManager extends JpaRepository<Person, Integer> {

    <S extends Person> S save(S person);
    void delete(Person person);
    void deleteById(int id);
    List<Person> findAll();
    List<Person> findByNickContains(String s);

    Boolean existsByNick(String nick);
    Person getByNick(String nick);

    Person getById(int id);
}
