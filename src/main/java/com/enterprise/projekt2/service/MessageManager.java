package com.enterprise.projekt2.service;

import com.enterprise.projekt2.domain.Message;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageManager extends JpaRepository<Message, Integer> {
    List<Message> findAllByPerson_Id(int id);
    void deleteById(int id);
    List<Message> findAllByContentContainsAndPerson_NickContains(String content, String nick);
}
